import logging
import os

API_V1_STR = '/api/v1'

PROJECT_NAME = os.getenv('PROJECT_NAME')

POSTGRES_HOST = os.getenv('POSTGRES_HOST')
POSTGRES_USER = os.getenv('POSTGRES_USER')
POSTGRES_PASSWORD = os.getenv('POSTGRES_PASSWORD')
POSTGRES_DB = os.getenv('POSTGRES_DB')
SQLALCHEMY_DATABASE_URI = (
    f'postgresql://{POSTGRES_USER}:{POSTGRES_PASSWORD}@{POSTGRES_HOST}/{POSTGRES_DB}'
)

ELASTIC_HOST = os.getenv('ELASTIC_HOST')
ELASTIC_URI = f'{ELASTIC_HOST}:9200'

REDIS_HOST = os.getenv('REDIS_HOST')

CRWL_API_KEY = os.getenv('CRWL_API_KEY')

BACKEND_CORS_ORIGINS = os.getenv('BACKEND_CORS_ORIGINS')

levels = {
    'CRITICAL' : logging.CRITICAL,
    'ERROR' : logging.ERROR,
    'WARNING' : logging.WARNING,
    'INFO' : logging.INFO,
    'DEBUG' : logging.DEBUG
}

LOG_LEVEL = levels[os.getenv('LOG_LEVEL')]
