import logging
import re
from datetime import datetime

from app.core import config
from app.crwl_api.crwl import Crwl
from app.models.ad_car import AdCar
from app.db.session import Session
from app.search_engine.elasticsearch import ElasticsearchEngine
from app.repository.ad_car_repository import AdCarRepository

logging.basicConfig(level=config.LOG_LEVEL)


def crwl_results_to_models(crwl_results):
    res = []
    for crwl_res in crwl_results:
        crwl_res['ad_id'] = crwl_res.pop('Id')
        crwl_res['region_id'] = crwl_res.pop('region_Id')

        # Sometimes enginevol has value like "105.."
        # This should fix it
        match = re.match('(\d+)\.\.', crwl_res['enginevol'])
        if match:
            crwl_res['enginevol'] = None
            if crwl_res['modification'] and 'л.с.' not in crwl_res['modification']:
                crwl_res['modification'] = '{} {} л.с.'.format(crwl_res['modification'], match.group(1))
            else:
                crwl_res['modification'] = '{} л.с.'.format(match.group(1))

        try:
            res.append(AdCar(**crwl_res))
        except Exception as e:
            logging.error('Can\'t convert crwl_res to AdCar')
            logging.error(crwl_res)
            logging.error(e)
    return res


def create_ads_for_period(date_start, date_end):
    repository = AdCarRepository(Session(), ElasticsearchEngine())
    crwl_api = Crwl()
    regions = crwl_api.get_regions()
    for region in regions:
        crwl_results = crwl_api.get_ads(region['Id'], date_start, date_end)
        if not crwl_results:
            continue
        ad_cars = crwl_results_to_models(crwl_results)
        for ad_car in ad_cars:
            try:
                repository.add_ad(ad_car)
            except Exception as e:
                logging.error(e)


def create_new_ads():
    logging.info('Start create_new_ads task {}'.format(datetime.now().strftime('%Y-%m-%d %H:%M:%S')))
    successful_create = 0
    total_ads = 0

    repository = AdCarRepository(Session(), ElasticsearchEngine())
    crwl_api = Crwl()
    regions = crwl_api.get_regions()
    for region in regions:
        crwl_results = crwl_api.get_new_ads(region['Id'])
        if not crwl_results:
            continue
        ad_cars = crwl_results_to_models(crwl_results)
        for ad_car in ad_cars:
            try:
                total_ads += 1
                repository.add_ad(ad_car)
                successful_create += 1
            except Exception as e:
                logging.error(e)

    logging.info('{} from {} were successful created'.format(successful_create, total_ads))
    logging.info('End task {}'.format(datetime.now().strftime('%Y-%m-%d %H:%M:%S')))
