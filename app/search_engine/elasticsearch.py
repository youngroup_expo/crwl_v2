from elasticsearch import Elasticsearch

from app.core.config import ELASTIC_URI
from app.search_engine.models import AdCar


class ElasticsearchEngine:
    def __init__(self, elastic_uri=ELASTIC_URI):
        self._es = Elasticsearch([elastic_uri])

    def index_ad(self, ad: AdCar):
        self._es.index(index='crwl', body=ad.dict())

    def find_highest_price_ads(
            self,
            *,
            mark,
            model,
            year=None,
            date_start=None,
            date_end=None,
            sources=None,
            limit=5):
        sort = ['_score', {'price': 'desc'}]
        must = [
            {'match': {'marka': mark}},
            {'match': {'model_full': model}}
        ]
        bool_ = {'must': must}

        term = None
        if year:
            term = {'term': {'year': year}}

        terms = None
        if sources:
            terms = {'terms': {'source': sources}}

        range_ = None
        if date_start or date_end:
            dt = {}
            if date_start:
                dt['gte'] = date_start

            if date_end:
                dt['lte'] = date_end

            range_ = {'range': {'dt': dt}}

        if term or terms or range_:
            filter_ = [el for el in [term, terms, range_] if el]
            bool_['filter'] = filter_

        body = {
            'size': limit,
            'sort': sort,
            'query': {'bool': bool_}
        }

        search_res = self._es.search(index='crwl', search_type='dfs_query_then_fetch', body=body)
        res = map(lambda doc: AdCar(**doc['_source']), search_res['hits']['hits'])

        return res
