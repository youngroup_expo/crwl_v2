from pydantic import BaseModel, validator
from datetime import datetime
from typing import Optional


class AdCar(BaseModel):
    ad_id: int
    price: int
    marka: str
    model_full: str
    year: int
    enginevol: Optional[float]
    dt: datetime
    run: int
    region_id: str
    source: str

    @validator('enginevol', pre=True)
    def _replace_empty(cls, v):
        if v == '':
            v = None
        return v
