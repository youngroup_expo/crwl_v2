import datetime
from fastapi import APIRouter, Depends, HTTPException, Query
from typing import List, Optional

from app.api.utils.ad_car_repository import get_ad_car_repository
from app.repository.ad_car_repository import AdCarRepository
from app.models.ad_short_info import AdShortInfo

router = APIRouter()


@router.get("/price/top", tags=["price"], response_model=List[AdShortInfo])
def top_prices(
        mark: str,
        model: str,
        year: int = None,
        date_start: datetime.date = None,
        date_end: datetime.date = None,
        sources: List[str] = Query(None),
        limit=5,
        repository: AdCarRepository = Depends(get_ad_car_repository)):
    arguments = locals()
    arguments.pop('repository')

    return repository.find_highest_price_ads(**arguments)
