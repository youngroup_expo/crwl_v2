from fastapi import APIRouter

from app.api.api_v1.endpoints import price

api_router = APIRouter()
api_router.include_router(price.router)
