from starlette.requests import Request

from app.repository.ad_car_repository import AdCarRepository
from app.search_engine.elasticsearch import ElasticsearchEngine


def get_ad_car_repository(request: Request):
    return AdCarRepository(request.state.db, ElasticsearchEngine())
