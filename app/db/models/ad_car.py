import datetime
from sqlalchemy import BigInteger, Column, DateTime, Float, Integer, String
from sqlalchemy.ext.declarative import declared_attr

from app.db.models.base import Base


class AdCar(Base):

    @declared_attr
    def __tablename__(cls):
        return 'ad_car'

    ad_id = Column(BigInteger, primary_key=True, unique=True)
    address = Column(String)
    average_price = Column(Integer)
    body = Column(String)
    color = Column(String)
    company = Column(String)
    complect = Column(String)
    condition = Column(String)
    difference_price = Column(Integer)
    drive = Column(String)
    dt = Column(DateTime, nullable=False)
    e_mail = Column(String)
    engine = Column(String)
    enginevol = Column(String)
    fast = Column(String)
    fio = Column(String)
    fuel = Column(String)
    gender = Column(String)
    horse = Column(String)
    info = Column(String)
    latitude = Column(Float(53))
    longitude = Column(Float(53))
    marka = Column(String)
    metro = Column(String)
    model = Column(String)
    model_2 = Column(String)
    modification = Column(String)
    new = Column(String)
    phone = Column(String)
    phone_find = Column(String)
    photo = Column(String)
    price = Column(Integer)
    protected = Column(String)
    pts_owner = Column(String)
    region = Column(String)
    region_id = Column(Integer, nullable=False)
    run = Column(Integer)
    run_ed = Column(String)
    source = Column(String, nullable=False)
    transmission = Column(String)
    url = Column(String)
    viewed = Column(String)
    vin = Column(String)
    wheel = Column(String)
    year = Column(String)
    ts = Column(DateTime, nullable=False, default=datetime.datetime.utcnow)
