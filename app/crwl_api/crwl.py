import requests
from urllib.parse import urljoin

from app.core.config import CRWL_API_KEY


class CrwlAPIException(Exception):
    pass


class Crwl(object):
    BASE_URL = 'http://crwl.ru/api/rest/latest/'

    def __init__(self, api_key=CRWL_API_KEY):
        self._api_key = api_key

    def get_ads(self, region=None, date_start=None, date_end=None):
        url = urljoin(self.BASE_URL, 'get_ads/')

        params = {'api_key': self._api_key, 'format': 'json'}
        if date_start:
            params['min_date'] = date_start.strftime('%d-%m-%Y')
        if date_end:
            params['max_date'] = date_end.strftime('%d-%m-%Y')
        if region:
            params['region'] = region

        try:
            response = requests.get(url, params=params)
            data = self._handle_response(response)
        except Exception as e:
            raise CrwlAPIException from e

        return data

    def get_new_ads(self, region=None):
        url = urljoin(self.BASE_URL, 'get_new_ads/')

        params = {'api_key': self._api_key, 'format': 'json'}
        if region:
            params['region'] = region

        try:
            response = requests.get(url, params=params)
            data = self._handle_response(response)
        except Exception as e:
            raise CrwlAPIException from e

        return data

    def get_regions(self):
        url = urljoin(self.BASE_URL, 'regions/')
        params = {'api_key': self._api_key, 'format': 'json'}

        try:
            response = requests.get(url, params=params)
            data = self._handle_response(response)
        except Exception as e:
            raise CrwlAPIException from e

        return data

    def _handle_response(self, response):
        if response.status_code != 200:
            raise CrwlAPIException('{}: {}'.format(response.status_code, response.reason))

        if len(response.content) < 1:
            return

        data = response.json()
        if 'status' in data and data['status'].lower() == 'failed':
            raise CrwlAPIException(data.get('msg'))

        return data
