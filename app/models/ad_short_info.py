from pydantic import BaseModel, validator
from datetime import datetime
from typing import Optional


class AdShortInfo(BaseModel):
    url: str
    marka: str
    model: str
    model_2: str
    color: str
    body: str
    enginevol: Optional[float]
    condition: str
    run: int
    year: int
    modification: str
    price: int
    vin: str
    average_price: int
    dt: datetime

    @validator('enginevol', pre=True)
    def _replace_empty(cls, v):
        if v == '':
            v = None
        return v