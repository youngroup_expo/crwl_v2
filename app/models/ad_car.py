from pydantic import BaseModel, validator
from datetime import datetime
from typing import Optional

class AdCar(BaseModel):
    # Fields description http://crwl.ru/api/
    ad_id: int
    url: str
    dt: datetime
    marka: str
    model: str
    model_2: str
    color: str
    body: str
    transmission: str
    engine: str
    enginevol: Optional[float]
    condition: str
    drive: str
    run: int
    run_ed: str
    year: int
    modification: str
    phone: str
    fio: str
    address: str
    price: int
    viewed: Optional[int]
    pts_owner: Optional[int]
    info: str
    horse: Optional[float]
    vin: str
    e_mail: Optional[str]
    company: bool
    fuel: str
    photo: str
    latitude: Optional[float]
    longitude: Optional[float]
    wheel: str
    complect: str
    phone_find: int
    metro: str
    region: str
    region_id: str
    average_price: int
    difference_price: int
    source: str
    fast: str
    protected: str
    new: bool
    gender: str

    @validator('enginevol', 'horse', 'latitude', 'longitude', 'viewed', pre=True)
    def _replace_empty(cls, v):
        if v == '':
            v = None
        return v
