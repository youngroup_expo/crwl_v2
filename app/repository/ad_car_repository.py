from typing import List

from app.db.models.ad_car import AdCar as DbAdCar
from app.models.ad_car import AdCar
from app.search_engine.models import AdCar as EsAdCar


class AdCarRepository:
    def __init__(self, db_session, search_engine):
        self._db_session = db_session
        self._search_engine = search_engine

    def _ad_car_to_elastic(self, ad: AdCar):
        es_ad_args = {k: v for k, v in ad.dict().items() if k in EsAdCar.__annotations__.keys()}
        model_full_elems = [el for el in [ad.model, ad.model_2, ad.modification] if el]
        es_ad_args['model_full'] = ' '.join(model_full_elems)
        return EsAdCar(**es_ad_args)

    def add_ad(self, ad: AdCar):
        db_ad_car = DbAdCar(**ad.__values__)
        try:
            self._db_session.add(db_ad_car)
            self._db_session.commit()
        except Exception as e:
            self._db_session.rollback()
            raise e

        es_ad_car = self._ad_car_to_elastic(ad)
        self._search_engine.index_ad(es_ad_car)

    def find_highest_price_ads(
            self,
            *,
            mark,
            model,
            year=None,
            date_start=None,
            date_end=None,
            sources=None,
            limit=5) -> List[AdCar]:
        arguments = locals()
        arguments.pop('self')

        search_res = self._search_engine.find_highest_price_ads(**arguments)
        ad_ids = map(lambda ad: ad.ad_id, search_res)
        db_ads = self._db_session.query(DbAdCar).filter(DbAdCar.ad_id.in_(ad_ids)).all()

        return db_ads
