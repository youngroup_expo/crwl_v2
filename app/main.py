from datetime import datetime
from fastapi import FastAPI
from redis import Redis
from rq_scheduler import Scheduler

from starlette.middleware.cors import CORSMiddleware
from starlette.requests import Request

from app.api.api_v1.router import api_router
from app.core import config
from app.db.session import Session
from app.tasks import create_new_ads

app = FastAPI(title=config.PROJECT_NAME)

# CORS
origins = []

# Set all CORS enabled origins
if config.BACKEND_CORS_ORIGINS:
    origins_raw = config.BACKEND_CORS_ORIGINS.split(",")
    for origin in origins_raw:
        use_origin = origin.strip()
        origins.append(use_origin)
    app.add_middleware(
        CORSMiddleware,
        allow_origins=origins,
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    ),

app.include_router(api_router, prefix=config.API_V1_STR)


@app.on_event("startup")
async def startup_event():
    redis = Redis(host=config.REDIS_HOST)
    scheduler = Scheduler(connection=redis)
    scheduler.schedule(
        scheduled_time=datetime.utcnow(),
        func=create_new_ads,
        interval=60,
        repeat=None,
    )


@app.middleware("http")
async def db_session_middleware(request: Request, call_next):
    request.state.db = Session()
    response = await call_next(request)
    request.state.db.close()
    return response
