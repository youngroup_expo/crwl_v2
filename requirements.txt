certifi==2019.3.9
chardet==3.0.4
Click==7.0
croniter==0.3.30
dataclasses==0.6
elasticsearch==7.0.0
fastapi==0.13.0
h11==0.8.1
httptools==0.0.13
idna==2.8
psycopg2-binary==2.8.2
pydantic==0.21
python-dateutil==2.8.0
redis==3.2.1
requests==2.21.0
rq==1.0
rq-scheduler==0.9
six==1.12.0
SQLAlchemy==1.3.3
starlette==0.11.1
urllib3==1.24.1
uvicorn==0.6.1
uvloop==0.12.2
websockets==7.0
