import os
import psycopg2
import sys
import time
from psycopg2.extras import DictCursor

from elasticsearch import Elasticsearch


def main():
    start_time = time.time()
    conn_url = 'postgresql://{username}:{password}@{host}:{port}/{dbname}'.format(
        username=os.getenv("POSTGRES_USER"),
        password=os.getenv("POSTGRES_PASSWORD"),
        host=os.getenv("POSTGRES_HOST"),
        port='5432',
        dbname=os.getenv("POSTGRES_DB"))

    conn = psycopg2.connect(conn_url)
    cursor = conn.cursor(name='Fetch cursor', cursor_factory=DictCursor)
    cursor.itersize = 5000
    cursor.execute('SELECT * FROM ad_car')
    es_host = os.getenv("ELASTIC_HOST")
    es_connect_str = es_host + ':9200'
    es = Elasticsearch([es_connect_str])

    attempt = 1
    while not es.ping():
        print('No elastic connection. Attempt {} of 10'.format(attempt))
        if attempt == 10:
            return
        attempt += 1
    print('Successful elastic connection')


    es.indices.create(index='crwl', body={
        'settings':{
          'analysis': {
            'analyzer': {
              'lowercasespaceanalyzer': {
                'type': 'custom',
                'tokenizer': 'whitespace',
                'filter': [
                  'lowercase'
                ],
                'char_filter': [
                  'parentheses_filter'
                ]
              }
            },
            'char_filter': {
              'parentheses_filter': {
                'type': 'pattern_replace',
                'pattern': '[()]',
                'replacement': ''
              }
            }
          }
        },
        'mappings': {
            'properties': {
                'ad_id': {'type': 'long'},
                'price': {'type': 'long'},
                'marka': {'type': 'text', 'analyzer': 'lowercasespaceanalyzer'},
                'model_full': {'type': 'text', 'analyzer': 'lowercasespaceanalyzer'},
                'year': {'type': 'short'},
                'enginevol': {'type': 'half_float'},
                'dt': {'type': 'date'},
                'run': {'type': 'integer'},
                'region_id': {'type': 'text'},
                'source': {'type': 'text'}
            }
        }
    })

    count = 1
    while True:
        # consume result over a series of iterations
        # with each iteration fetching 2000 records
        records = cursor.fetchmany(size=5000)
        if not records:
            break

        for r in records:
            model_full_elems = [el for el in [r['model'], r['model_2'], r['modification']] if el]
            body = {
                'ad_id': r['ad_id'],
                'price': r['price'],
                'marka': r['marka'],
                'model_full': ' '.join(model_full_elems),
                'year': r['year'],
                'enginevol': r['enginevol'],
                'dt': r['dt'],
                'run': r['run'],
                'region_id': r['region_id'],
                'source': r['source'],
            }
            try:
                es.index(index='crwl', body=body)
            except Exception:
                print(body)

            count += 1
            if count % 5000 == 0:
                print('{} row - {} seconds'.format(count, time.time() - start_time))
                sys.stdout.flush()

    cursor.close()  # don't forget to cleanup
    conn.close()


if __name__ == "__main__":
    main()
