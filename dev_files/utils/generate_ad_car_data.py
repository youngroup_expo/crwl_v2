import json
import os
import psycopg2
import time
from random import randint


def random_with_N_digits(n):
    range_start = 10 ** (n - 1)
    range_end = (10 ** n) - 1
    return randint(range_start, range_end)


connection_parameters = {
    'host': 'db',
    'database': 'postgres',
    'user': 'postgres',
    'password': '111111'
}

for attempt in range(1,11):
    try:
        conn = psycopg2.connect(**connection_parameters)
        print('Successful postgresql connection')
        break
    except:
        print('No postgresql connection. Attempt {} of 10'.format(attempt))
        time.sleep(5)

cur = conn.cursor()

bad_models = [
    ('Suzuki', 'Grand Vitara'),
    ('Suzuki', 'Vitara'),
    ('Opel', 'Astra'),
]

with open(os.path.join('..', 'data', 'ad_car_data.json')) as file:
    data = json.loads(file.read())

for ad in data:
    ad['ad_id'] = ad.pop('Id')
    insert = 'INSERT INTO ad_car (' + ', '.join(ad.keys()) + ') VALUES (' + ', '.join(
        map(lambda x: '%(' + x + ')s', ad.keys())) + ')'
    cur.execute(insert, ad)

for ad_zip in zip(data, bad_models):
    ad = ad_zip[0]
    ad['marka'] = ad_zip[1][0]
    ad['model'] = ad_zip[1][1]
    ad['ad_id'] = random_with_N_digits(8)
    insert = 'INSERT INTO ad_car (' + ', '.join(ad.keys()) + ') VALUES (' + ', '.join(
        map(lambda x: '%(' + x + ')s', ad.keys())) + ')'
    cur.execute(insert, ad)

conn.commit()
cur.close()
conn.close()
