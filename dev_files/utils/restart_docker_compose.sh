docker-compose.exe -f docker-compose-dev.yml down
docker-compose.exe -f docker-compose-dev.yml build
docker-compose.exe -f docker-compose-dev.yml up -d db
docker-compose.exe -f docker-compose-dev.yml up -d elasticsearch
sleep 10
docker-compose.exe -f docker-compose-dev.yml up