#!/bin/sh
cd dev_files/utils
python generate_ad_car_data.py
python create_elastic_index.py
cd ../..
uvicorn app.main:app --host 0.0.0.0 --port 8000